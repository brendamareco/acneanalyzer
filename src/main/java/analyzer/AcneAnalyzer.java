package analyzer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import meitaSkinCare.Analyzer;

public class AcneAnalyzer implements Analyzer
{

	@Override
	public Map<String, Boolean> analyze(Set<String> ingredients) 
	{
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		if (ingredients.contains("aqua"))
			result.put("aqua", true);
		if (ingredients.contains("alcohol"))
			result.put("alcohol", false);
		if (ingredients.contains("paraben"))
			result.put("paraben", false);
		if (ingredients.contains("glycerin"))
			result.put("glycerin", true);
		
		return result;
	}

	@Override
	public String getName() 
	{
		return "acne";
	}
	
	@Override
	public String toString()
	{
		return getName();
	}
}